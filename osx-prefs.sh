#!/usr/bin/env bash
set -x

NAME=$1

if [ -z "$NAME" ]; then
  echo "Usage: $0 <name-of-machine>"
  exit 1
fi

###############################################################################
# System Preferences > General                                                #
###############################################################################

# Use a dark menu bar / dock
defaults write NSGlobalDomain AppleInterfaceStyle -string "Dark"

# Click in the scroll bar to jump to the spot that's clicked
defaults write -g AppleScrollerPagingBehavior -bool true


###############################################################################
# System Preferences > Dock                                                   #
###############################################################################

# Size of the icons in the Dock
defaults write com.apple.dock tilesize -int 48

# Double-click a window's title bar to minimize
defaults write NSGlobalDomain AppleMiniaturizeOnDoubleClick -bool false

# Enable auto-hide
defaults write com.apple.dock autohide -bool true

# Remove show/hide delay
defaults write com.apple.dock autohide-delay -float 0

# restart Dock
killall Dock


###############################################################################
# System Preferences > Language & Text                                        #
###############################################################################

# "Set language and text formats"
defaults write NSGlobalDomain AppleLanguages -array "en"
defaults write NSGlobalDomain AppleLocale -string "sv_SE@currency=SEK"
defaults write NSGlobalDomain AppleMeasurementUnits -string "Centimeters"
defaults write NSGlobalDomain AppleMetricUnits -bool true


###############################################################################
# System Preferences > Keyboard                                               #
###############################################################################

# Enable character repeat on keydown
defaults write -g ApplePressAndHoldEnabled -bool false

# Set a shorter Delay until key repeat
defaults write NSGlobalDomain InitialKeyRepeat -int 15

# Set a fast keyboard repeat rate
defaults write NSGlobalDomain KeyRepeat -int 1


###############################################################################
# System Preferences > Sharing                                                #
###############################################################################

# Set computer name
# Change the name within quotes to whatever you want
sudo scutil --set ComputerName "${NAME}"
sudo scutil --set HostName "${NAME}"
sudo scutil --set LocalHostName "${NAME}"
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string "${NAME}"


###############################################################################
# Misc.                                                                       #
###############################################################################

#Add a context menu item for showing the Web Inspector in web views
defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

#Show the ~/Library folder
chflags nohidden ~/Library && xattr -d com.apple.FinderInfo ~/Library

# Expand save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

# Save to disk (not to iCloud) by default
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

# Disable the “Are you sure you want to open this application?” dialog
defaults write com.apple.LaunchServices LSQuarantine -bool false

# Disable smart quotes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

# Disable smart dashes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

# "Prevent Time Machine from prompting to use new hard drives as backup volume"
defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

# Don't want Photos.app to open up as soon as you plug something in?
defaults write com.apple.ImageCapture disableHotPlug -bool YES

# Enable AirDrop over Ethernet
defaults write com.apple.NetworkBrowser BrowseAllInterfaces 1

# Change save destination for screenshots
defaults write com.apple.screencapture location ~/Desktop/Screenshots


###############################################################################
# Finder                                                                      #
###############################################################################

# "Use current directory as default search scope in Finder"
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# "Show Path bar in Finder"
defaults write com.apple.finder ShowPathbar -bool true

# Show Status bar in Finder
defaults write com.apple.finder ShowStatusBar -bool true

# Set default Finder location to home folder (~/)
defaults write com.apple.finder NewWindowTarget -string "PfLo" && \
defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}"

# Avoid creating .DS_Store files on network volumes
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

# "Enable snap-to-grid for desktop icons"
/usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist

# "Use column view in all Finder windows by default"
# Four-letter codes for the other view modes: `icnv`, `clmv`, `Flwv`
defaults write com.apple.finder FXPreferredViewStyle -string "clmv"

# Disable the warning when changing a file extension
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

# Show all filename extensions in Finder
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# allow text selection in Quick Look
defaults write com.apple.finder QLEnableTextSelection -bool true

# Enable spring loading for directories
defaults write NSGlobalDomain com.apple.springing.enabled -bool true

# Set sleep time
sudo pmset -a displaysleep 20 disksleep 15 sleep 60

# Set scroll direction
defaults write -g com.apple.swipescrolldirection -bool FALSE

# Always show date in menubar
defaults write com.apple.menuextra.clock ShowDate -bool true