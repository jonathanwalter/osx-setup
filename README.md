### Setting up OS X

These are some scripts for automating the setup of a (new) Mac. I've stolen stuff from _everywhere_.

Files in this package:

* `Brewfile` - installs [Homebrew](http://brew.sh/) taps, formulae, casks, fonts and Mac App Stope apps.
* `clean-dock.sh` - removes _everything_ from the Dock
* `osx-prefs.sh` - sets up the preferences how I like 'em

#### Installation

Install [Homebrew](http://brew.sh/) first of all. Then run the following commands.

1. `bash ./osx-prefs.sh`
2. `bash ./clean-dock.sh`
3. `brew bundle`

You have to run `clean-dock.sh` as your own user (i.e. not `root`).
